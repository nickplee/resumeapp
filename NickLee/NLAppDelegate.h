//
//  NLAppDelegate.h
//  NickLee
//
//  Created by Nick Lee on 4/13/14.
//  Copyright (c) 2013 Nicholas Lee Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NLRootViewController;

@interface NLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NLRootViewController *viewController;

@end
