//
//  NLViewController.h
//  NickLee
//
//  Created by Nick Lee on 4/13/14.
//  Copyright (c) 2013 Nicholas Lee Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface NLRootViewController : UIViewController

@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, weak) IBOutlet UIView *topView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topViewConstraint;

- (IBAction)tapButton:(id)sender;
- (IBAction)buttonDown:(id)sender;
- (IBAction)buttonUP:(id)sender;

@end
