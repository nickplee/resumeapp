//
//  NLViewController.m
//  NickLee
//
//  Created by Nick Lee on 4/13/14.
//  Copyright (c) 2013 Nicholas Lee Designs, LLC. All rights reserved.
//

#import "NLRootViewController.h"
#import <MessageUI/MessageUI.h>

@interface NLRootViewController () <MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) AVPlayerItem *playerItem;
@property (nonatomic, strong) AVPlayerLayer *playerLayer;
@property (nonatomic, weak) UIView *currentContentView;
@property (nonatomic) BOOL transitioning;

- (void)setupVideoPlayer;
- (AVAsset *)assetCompositionWithURL:(NSURL *)mURL;

@end

@implementation NLRootViewController

- (void)dealloc
{
    _topViewConstraint = nil;
    _topView  = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self setupVideoPlayer];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

#pragma mark - status bar

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - video player

- (void)setupVideoPlayer
{
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"spinny" withExtension:@"mov"];
    
    AVAsset *composition = [self assetCompositionWithURL:url];
    
    self.playerItem = [AVPlayerItem playerItemWithAsset:composition];
    self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
    
    self.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:self.playerItem];
    
    self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
    CGRect frame = CGRectMake(0, 0, 768, 1024);
    self.playerLayer.frame = frame;
    self.playerLayer.rasterizationScale = [[UIScreen mainScreen] scale];
    self.playerLayer.shouldRasterize = YES;
    self.playerLayer.minificationFilter = kCAFilterNearest;
    self.playerLayer.magnificationFilter = kCAFilterNearest;
    self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    self.playerLayer.transform = CATransform3DMakeScale(1.02f, 1.02f, 1.f); //to avoid bars on edges from NN filter
    [self.view.layer insertSublayer:self.playerLayer atIndex:0];
    
    [self.player play];
}

- (AVAsset *)assetCompositionWithURL:(NSURL *)mURL {
    
    int numOfCopies = 512;
    AVMutableComposition *composition = [[AVMutableComposition alloc] init];
    AVURLAsset* sourceAsset = [AVURLAsset URLAssetWithURL:mURL options:nil];
    // calculate time
    CMTimeRange editRange = CMTimeRangeMake(CMTimeMake(0, 600), CMTimeMake(sourceAsset.duration.value, sourceAsset.duration.timescale));
    NSError *editError;
    BOOL result = [composition insertTimeRange:editRange ofAsset:sourceAsset atTime:composition.duration error:&editError];
    
    if (result) {
        for (int i = 0; i < numOfCopies; i++) {
            [composition insertTimeRange:editRange ofAsset:sourceAsset atTime:composition.duration error:&editError];
        }
    }
    return composition;
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

#pragma mark - action

- (IBAction)buttonDown:(id)sender
{
    [sender setBackgroundColor:[UIColor whiteColor]];
    [sender setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}

- (IBAction)buttonUP:(id)sender
{
    [sender setBackgroundColor:[UIColor colorWithWhite:0.f alpha:.5f]];
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (IBAction)tapButton:(id)sender
{
    [sender setBackgroundColor:[UIColor colorWithWhite:0.f alpha:.5f]];
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    if ([sender tag] < 2) {
        //load an HTML page
        static NSString * const filenames[2] = {
            @"portfolio",
            @"resume",
        };
        
        NSString *filename = filenames[[sender tag]];
        NSURL *url = [[NSBundle mainBundle] URLForResource:filename withExtension:@"html"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        CGRect frame = [[UIScreen mainScreen] bounds];
        frame.origin.y += 44.f;
        frame.size.height -= 44.f;
        UIWebView *webView = [[UIWebView alloc] initWithFrame:frame];
        [webView loadRequest:request];
        webView.dataDetectorTypes = UIDataDetectorTypeNone;
        webView.opaque = NO;
        webView.backgroundColor = [UIColor clearColor];
        
        for (UIView *aview in [webView subviews]) {
            if ([aview isKindOfClass:[UIScrollView class]]) {
                // Uncomment this line to make the scroll bars white
                // [(UIScrollView *)view setIndicatorStyle:UIScrollViewIndicatorStyleWhite];
                for (UIView *innerView in [aview subviews]) {
                    if ([innerView isKindOfClass:[UIImageView class]] &&
                        innerView.frame.size.width != 5.0) innerView.hidden = YES;
                }
            }
        }
        
        [self presentContentView:webView];
    }
    else {
        //mail composer!
        
        if ([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *vc = [[MFMailComposeViewController alloc] init];
            [vc setToRecipients:@[@"nick@nickplee.com"]];
            [vc setMailComposeDelegate:self];
            [self presentViewController:vc animated:YES completion:nil];
        }
        else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Your device is not configured to send mail.  Please set up an email account and try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alertView show];
        }
    }
    
    
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - view shuffling

static CFTimeInterval const animationMultiplier = 1.0;

//lots of magic numbers here!
- (void)presentContentView:(UIView *)view
{
    if (self.currentContentView || self.transitioning) return;
    
    self.transitioning = YES;
    
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"rasterizationScale"];
    anim.fromValue = @([[UIScreen mainScreen] scale]);
    anim.toValue = @(0.055f);
    anim.duration = 0.6 * animationMultiplier;
    anim.fillMode = kCAFillModeForwards;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    anim.removedOnCompletion = NO;
    [self.playerLayer addAnimation:anim forKey:@"rasterizationScale"];
    
    anim = [CABasicAnimation animationWithKeyPath:@"opacity"];
    anim.fromValue = @(1.f);
    anim.toValue = @(0.5f);
    anim.duration = 0.6 * animationMultiplier;
    anim.fillMode = kCAFillModeForwards;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    anim.removedOnCompletion = NO;
    [self.playerLayer addAnimation:anim forKey:@"opacity"];
    
    static const CGFloat padding = 0.f;
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(padding, 1024+padding, 768-padding, 1024-padding)];
    [contentView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [contentView setAlpha:0.f];
    [self.view addSubview:contentView];
    
    [contentView addSubview:view];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.75f]];
    [button setOpaque: NO];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setFrame:CGRectMake(0, 0, 768, 44)];
    
    [button addTarget:self action:@selector(dismisscontentView:) forControlEvents:UIControlEventTouchUpInside];
    [button addTarget:self action:@selector(buttonDown:) forControlEvents:UIControlEventTouchDown];
    [button addTarget:self action:@selector(buttonUP:) forControlEvents:UIControlEventTouchUpOutside | UIControlEventTouchCancel];
    
    [button setTitle:@"back" forState:UIControlStateNormal];
    [[button titleLabel] setFont:[UIFont fontWithName:@"CourierNewPS-BoldMT" size:15.f]];
    [contentView addSubview:button];
    
    UIColor *color = [self.topView.backgroundColor colorWithAlphaComponent:0.6];
    
    [contentView setBackgroundColor:color];
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismisscontentView)];
    [swipe setDirection:UISwipeGestureRecognizerDirectionDown];
    
    [contentView addGestureRecognizer:swipe];
    
    [UIView animateWithDuration:0.38*animationMultiplier delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [contentView setFrame:CGRectMake(padding, 247+padding, 768-(padding*2), 1024-(padding*2))];
        [contentView setAlpha:1.f];
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.22*animationMultiplier delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [contentView setFrame:CGRectMake(padding, padding, 768-(padding*2), 1024-(padding*2))];
            self.topViewConstraint.constant = -247.f;
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            self.currentContentView = contentView;
            self.transitioning = NO;
        }];
    }];
}

- (void)dismisscontentView:(id)sender
{
    [sender setBackgroundColor:[UIColor colorWithWhite:0.f alpha:.5f]];
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    if (!self.currentContentView) return;
    self.transitioning = YES;
    
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"rasterizationScale"];
    anim.fromValue = @(0.055f);
    anim.toValue = @([[UIScreen mainScreen] scale]);
    anim.duration = 0.6 * animationMultiplier;
    anim.fillMode = kCAFillModeForwards;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    anim.removedOnCompletion = NO;
    [self.playerLayer addAnimation:anim forKey:@"rasterizationScale"];
    
    anim = [CABasicAnimation animationWithKeyPath:@"opacity"];
    anim.fromValue = @(0.5f);
    anim.toValue =  @(1.f);
    anim.duration = 0.6 * animationMultiplier;
    anim.fillMode = kCAFillModeForwards;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    anim.removedOnCompletion = NO;
    [self.playerLayer addAnimation:anim forKey:@"opacity"];
    
    static const CGFloat padding = 0.f;
    
    [UIView animateWithDuration:0.22*animationMultiplier delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [self.currentContentView setFrame:CGRectMake(padding, 247+padding, 768-(padding*2), 1024-(padding*2))];
        self.topViewConstraint.constant = 0.0f;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.38*animationMultiplier  delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [self.currentContentView setFrame:CGRectMake(padding, 1024+padding, 768-padding, 1024-padding)];
            [self.currentContentView setAlpha:0.f];
        } completion:^(BOOL finished) {
            [self.currentContentView removeFromSuperview];
            self.currentContentView = nil;
            self.transitioning = NO;
        }];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
