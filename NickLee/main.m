//
//  main.m
//  NickLee
//
//  Created by Nick Lee on 4/13/14.
//  Copyright (c) 2013 Nicholas Lee Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NLAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NLAppDelegate class]));
    }
}
